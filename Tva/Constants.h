//
//  Constants.h
//  Tva
//
//  Created by Rea Bar on 24.5.2016.
//  Copyright © 2016 Admin. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

static NSString* CURRENT_AFFAIRS_AND_NEWS = @"אקטואליה וחדשות";
static NSString* SPORT = @"ספורט";
static NSString* MOVIE = @"סרט";
static NSString* SERIES = @"סדרה";
static NSString* CULTURE_ENTERTAINMENT_AND_MUSIC = @"תרבות בידור ומוסיקה";
static NSString* CHILDREN = @"ילדים";
static NSString* DOCUMENTARY_NATURE_AND_LEISURE = @"תעודה טבע ופנאי";

#endif /* Constants_h */
