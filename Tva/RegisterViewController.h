//
//  RegisterViewController.h
//  Tva
//
//  Created by Rea Bar on 28.12.2015.
//  Copyright © 2015 Admin. All rights reserved.
//

#import "ViewController.h"
#import "Model.h"
@interface RegisterViewController : ViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *userNameInput;
@property (weak, nonatomic) IBOutlet UITextField *passwordInput;
@property (weak, nonatomic) IBOutlet UITextField *reapeatPasswordInput;
@property (weak, nonatomic) IBOutlet UITextField *ageInput;
@property (weak, nonatomic) IBOutlet UITextField *cityInput;
@property (weak, nonatomic) IBOutlet UIButton *femaleButton;
@property (weak, nonatomic) IBOutlet UIButton *maleButton;

@property (weak, nonatomic) IBOutlet UIButton *dramaButton;
@property (weak, nonatomic) IBOutlet UIButton *sportButton;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet UIButton *horrorButton;
@property (weak, nonatomic) IBOutlet UIButton *docoButton;
@property (weak, nonatomic) IBOutlet UIButton *comedyButton;
- (IBAction)backButton:(id)sender;
- (IBAction)signupButton:(id)sender;
- (IBAction)dramaPressed:(id)sender;
- (IBAction)sportPressed:(id)sender;
- (IBAction)actionPressed:(id)sender;
- (IBAction)comedyPressed:(id)sender;
- (IBAction)documentaryPressed:(id)sender;
- (IBAction)horrorPressed:(id)sender;
- (IBAction)femalePressed:(id)sender;
- (IBAction)maleButton:(id)sender;
@property (nonatomic) BOOL isDrama;
@property (nonatomic) BOOL isHorror;
@property (nonatomic) BOOL isDoco;
@property (nonatomic) BOOL isComedy;
@property (nonatomic) BOOL isSport;
@property (nonatomic) BOOL isAction;
@property (nonatomic) BOOL isMale;
@property (nonatomic) BOOL isFemale;
@property (weak,nonatomic) NSString* gender;

@property NSMutableData *mutData;


@property (nonatomic) BOOL isValidPassWord;
@property (weak, nonatomic) IBOutlet UILabel *logo;
-(BOOL)validatePassword;
-(void)alert;
@end
