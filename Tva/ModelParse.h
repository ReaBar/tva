//
//  ModelParse.h
//  Tva
//
//  Created by Admin on 12/26/15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Model.h"
#import <Parse/Parse.h>
#import "User.h"

@interface ModelParse : NSObject
-(BOOL)logout;
-(BOOL)login:(NSString*)user pwd:(NSString*)pwd;
-(BOOL)signup:(NSString*)user pwd:(NSString*)pwd;
-(NSString*)getCurrentUser;
-(void)addNewUser:(NSString*)userName withPassword:(NSString*)password;
-(void)addUser:(User*)userName;
-(void)deleteUser:(User*)userName;
-(User*)getUser:(NSString*)userName;
-(NSArray*)getUsers;
-(User*)getUserDetails:(NSString*)userName password:(NSString*)passWord;
-(void)addCable:(NSString*)cable;
-(void)addAutomaticSystem: (BOOL)autoButton;
-(void)saveImage:(UIImage*)image withName:(NSString*)imageName;
-(UIImage*)getImage:(NSString*)imageName;
-(User*)parseUser;
-(void)addUserToDB:(NSString*)city age:(NSString*)age gender:(NSString*)gender drama:(BOOL)drama comedy:(BOOL)comedy action:(BOOL)action doco:(BOOL)doco sport:(BOOL)sport horror:(BOOL)horror;
-(NSArray*)getUserPrograms;
-(NSArray*)getPrograms;
-(NSArray*)getProgramsWithNSDateFormat;
-(NSArray*)getUserProgramsByHour:(NSString*)begin end:(NSString*)end;
-(NSArray*)getUserProgramsByHour:(NSDate*)begin;
-(void)addProgramToUser:(Program*)program;
-(void)deleteProgramFromUser:(Program*)program;
-(NSArray*)getProgramsFromDate:(NSString*)date;
-(NSArray*)getUserProgramsFromDate:(NSString*)date;
-(NSDate*)getTableLastUpdateTime:(NSString*)tableName;
-(NSDate*)getUserLastUpdate:(NSString*)username;
@property BOOL checkValidUserProgram;
-(BOOL)checkUserProgram;
-(void)addAutomaticPrograms;
-(void)removeAutomaticPrograms;
@end
