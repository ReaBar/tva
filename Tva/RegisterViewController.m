//
//  RegisterViewController.m
//  Tva
//
//  Created by Rea Bar on 28.12.2015.
//  Copyright © 2015 Admin. All rights reserved.
//

#import "RegisterViewController.h"
#import "AFNetworking.h"

@interface RegisterViewController ()

@end
Model *model;
User *user;

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIColor *color = [UIColor orangeColor];
    _logo.font = [UIFont fontWithName:@"FabfeltScript-Bold" size:5];
    _logo.text = @"TVa";
    [_logo setTextColor:color];
    _isDrama=NO;
    _isHorror=NO;
    _isDoco=NO;
    _isComedy=NO;
    _isSport=NO;
    _isAction=NO;
    _isMale=NO;
    _isFemale=NO;
   // _ageInput.keyboardType=UIKeyboardTypeNumberPad;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//
- (IBAction)backButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)signupButton:(id)sender {
    _isValidPassWord= [self validatePassword];
   
          if (_isValidPassWord==false || _gender==nil || (![_cityInput.text isEqualToString: [@"center" lowercaseString]] && ![self.cityInput.text isEqualToString: [@"north" lowercaseString]] && ![_cityInput.text isEqualToString: [@"south" lowercaseString]]) || [_ageInput.text intValue]>100 || [_ageInput.text intValue]<=0) {
        [self alert];
    }
    //if (_isValidPassWord && _gender!=nil)
    else{

        [[Model instance] signup:_userNameInput.text  pwd:_passwordInput.text city:_cityInput.text age:_ageInput.text gender:_gender drama:_isDrama comedy:_isComedy action:_isAction doco:_isDoco sport:_isSport horror:_isHorror block:^(BOOL res){
            if (!res) {
                [self alert];
            }
            else{
                [self dismissViewControllerAnimated:YES completion:nil];
                [[UIApplication sharedApplication]
                 openURL:[ NSURL URLWithString:@"http://www.google.com"]];
                [model addUserToDB:_cityInput.text age:_ageInput.text gender:_gender drama:_isDrama comedy:_isComedy action:_isAction doco:_isDoco sport:_isSport horror:_isHorror];
                
            }
        }];
        
        NSString* _true = @"true";
        NSString* _false = @"false";
        
        NSString* drama = _isDrama ? _true: _false;
        NSString* horror = _isHorror ? _true : _false;
        NSString* doco = _isDrama ? _true : _false;
        NSString* action = _isAction ? _true: _false;
        NSString* comedy = _isComedy ? _true : _false;
        NSString* sport = _isSport ? _true : _false;
        
        NSString* tempString = [NSString stringWithFormat:@"username=%@&age=%@&gender=%@&location=%@&genreDrama=%@&genreDoco=%@&genreHorror=%@&genreAction=%@&genreComedy=%@&genreSport=%@",_userNameInput.text,_ageInput.text,_gender,_cityInput.text,drama,doco,horror,action,comedy,sport];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        [manager GET:[NSString stringWithFormat:@"http://tranquil-dusk-24757.herokuapp.com/registration?%@",tempString] parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            NSLog(@"JSON: %@", responseObject);
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    }
}
         

- (IBAction)dramaPressed:(id)sender {
    if ([sender isSelected]) {
        _dramaButton.showsTouchWhenHighlighted = NO;
         _isDrama=NO;
        [sender setSelected:NO];
    }
    else{
        _dramaButton.showsTouchWhenHighlighted = YES;
        [sender setSelected:YES];
        _isDrama=YES;
    }
}

- (IBAction)sportPressed:(id)sender {
    if ([sender isSelected]) {
        _sportButton.showsTouchWhenHighlighted = NO;
         _isSport=NO;
        [sender setSelected:NO];
    }
    else{
        _sportButton.showsTouchWhenHighlighted = YES;
        _isSport=YES;
        [sender setSelected:YES];
    }
}

- (IBAction)actionPressed:(id)sender {
    if ([sender isSelected]) {
        _actionButton.showsTouchWhenHighlighted = NO;
         _isAction=NO;
        [sender setSelected:NO];
    }
    else{
        _actionButton.showsTouchWhenHighlighted = YES;
        _isAction=YES;
        [sender setSelected:YES];
    }
}

- (IBAction)comedyPressed:(id)sender {
    if ([sender isSelected]) {
        _comedyButton.showsTouchWhenHighlighted = NO;
         _isComedy=NO;
        [sender setSelected:NO];
    }
    else{
        _comedyButton.showsTouchWhenHighlighted = YES;
        _isComedy=YES;
        [sender setSelected:YES];
    }
}

- (IBAction)documentaryPressed:(id)sender {
    if ([sender isSelected]) {
        _docoButton.showsTouchWhenHighlighted = NO;
         _isDoco=NO;
        [sender setSelected:NO];
    }
    else{
        _docoButton.showsTouchWhenHighlighted = YES;
        _isDoco=YES;
        [sender setSelected:YES];
    }
}

- (IBAction)horrorPressed:(id)sender {
    if ([sender isSelected]) {
        _horrorButton.showsTouchWhenHighlighted = NO;
         _isHorror=NO;
        [sender setSelected:NO];
    }
    else{
        _horrorButton.showsTouchWhenHighlighted = YES;
        _isHorror=YES;
        [sender setSelected:YES];
    }
}

- (IBAction)femalePressed:(id)sender {
    if ([sender isSelected]) {
        _femaleButton.showsTouchWhenHighlighted = NO;
        _isFemale=NO;
        _gender=nil;
        [sender setSelected:NO];
    }
    else{
        if(_isMale==NO){
            _femaleButton.showsTouchWhenHighlighted = YES;
            _isFemale=YES;
            _gender=@"female";
            [sender setSelected:YES];

        }
           }

}

- (IBAction)maleButton:(id)sender {
    if ([sender isSelected]) {
        _maleButton.showsTouchWhenHighlighted = NO;
        _isMale=NO;
        _gender=nil;
        [sender setSelected:NO];
    }
    else{
        if (_isFemale==NO) {
            _maleButton.showsTouchWhenHighlighted = YES;
            _isMale=YES;
            _gender=@"male";
            [sender setSelected:YES];
        }
        
      
    }

    
    
}



-(void)alert{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Signup Error"
                                                                   message:@"one of the parameters is invalid. try again. "
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}
-(BOOL)validatePassword{
    if([_passwordInput.text isEqualToString:_reapeatPasswordInput.text]){
        return YES;
    }
    NSLog(@"Password doesn't match");
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Password Error"
                                                                   message:@"Password doesn't match"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    return NO;
}



-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.userNameText resignFirstResponder];
    [self.passWordText resignFirstResponder];
    [self.reapeatPasswordInput resignFirstResponder];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
@end
