//
//  Model.h
//  Tva
//
//  Created by Admin on 12/10/15.
//  Copyright © 2015 Admin. All rights reserved.
//............

#import <Foundation/Foundation.h>
#import "Program.h"
#import "User.h"
#import <UIKit/UIKit.h>
#import "ModelSql.h"

@interface Model : NSObject
@property NSString* user;
+(Model*) instance;
-(NSString*)getCurrentUser;
-(void)getUsersAsynch:(void(^)(NSArray*))blockListener;
-(void)getUserAsynch:(NSString*)userName block:(void(^)(User*))block;
-(void)deleteUserAsynch:(User*)userName block:(void(^)(NSError*))block;
-(void)addNewUserAsynch:(NSString*)userName withPassword:(NSString*)password block:(void(^)(NSError*))block;
-(void)addCableAsynch:(NSString*)cable block:(void(^)(NSError*))block;
-(void)addAutomaticButtonCableAsynch:(BOOL)autoButton block:(void(^)(NSError*))block;
-(void)getUserDetailsAsynch:(NSString*)userName password:(NSString*)passWord block:(void(^)(User*))block;
-(void)addUserAsynch:(User*)userName block:(void(^)(NSError*))block;
-(NSArray*) getAllPrograms;
-(void)login:(NSString*)user pwd:(NSString*)pwd block:(void(^)(BOOL))block;
-(void)signup:(NSString*)user pwd:(NSString*)pwd city:(NSString*)city age:(NSString*)age gender:(NSString*)gender drama:(BOOL)drama comedy:(BOOL)comedy action:(BOOL)action doco:(BOOL)doco sport:(BOOL)sport horror:(BOOL)horror block:(void(^)(BOOL))block;
-(void)logout:(void(^)(BOOL))block;
-(void)saveUserImage:(UIImage*)image block:(void(^)(NSError*))block;
-(void)getUserImage:(NSString*)username block:(void(^)(UIImage*))block;
-(void)parseUserAsynch:(void(^)(User*))block;
-(void)addUserToDB:(NSString*)city age:(NSString*)age gender:(NSString*)gender drama:(BOOL)drama comedy:(BOOL)comedy action:(BOOL)action doco:(BOOL)doco sport:(BOOL)sport horror:(BOOL)horror;
-(void)getUserProgramsAsynch:(void(^)(NSMutableArray*))block;
-(void)getProgramsAsynch:(void(^)(NSArray*))block;
-(void)getUserProgramsByHourAsynch:(NSString*)begin end:(NSString*)end block:(void(^)(NSArray*))block;
-(void)getUserProgramsByHourAsynch:(NSDate*)begin block:(void(^)(NSArray*))block;
-(void)addProgramToUserAsynch:(Program*)program block:(void(^)(NSError*))block;
-(void)deleteProgramFromUser:(Program*)program block:(void(^)(NSError*))block;
+(void)saveUserImage:(UIImage*)image withName:(NSString*)imageName;
-(void)addAutomaticPrograms: (void(^)(NSError*))block;
-(void)removeAutomaticPrograms: (void(^)(NSError*))block;
@end
