//
//  Program.m
//  Tva
//
//  Created by Admin on 12/10/15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import "Program.h"

@implementation Program

-(id)initWithName:(NSString*)name withObjectId:(NSString*)objectId cable:(NSString*)cable channel:(NSString*)channel genre:(NSString*)genre beginTime:(NSDate*)begin endTime:(NSDate*)end{
    self=[super init];
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"Asia/Jerusalem"];
    NSString* startTime = [formatter stringFromDate:begin];
    NSString* endTime = [formatter stringFromDate:end];
    if(self){
        _name=name;
        _channel = channel;
        _objectId = objectId;
        _startTime = [formatter dateFromString:startTime];
        _endTime = [formatter dateFromString:endTime];
        _genre = genre;
        _cable = cable;
    }
    [self parseDateToInt:begin andWithEndTime:end];
    return self;
}

-(id)initWithUserName:(NSString*)username withProgramName:(NSString*)programName withObjectId:(NSString*)objectId cable:(NSString*)cable channel:(NSString*)channel genre:(NSString*)genre beginTime:(NSDate*)begin endTime:(NSDate*)end{
    self = [super init];
    if(self){
        _userName = username;
        _objectId = objectId;
        _name = programName;
        _cable = cable;
        _channel = channel;
        _genre = genre;
        _startTime = begin;
        _endTime = end;
    }
    return self;
}
//-(id)init:(NSString*)username withObjectId:(NSString*)objectId programeName:(NSString*)name beginTime:(NSString*)begin endTime:(NSString*)end{
//    self=[super init];
//    if(self){
//        _userName = username;
//        _objectId = objectId;
//        _name = name;
//        _begin = begin;
//        _end = end;
//    }
//return self;
//}

- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[Program class]]) {
        Program * other = object;
        if(([self.name isEqualToString:other.name]) && ([self.startTime isEqualToDate:other.startTime]) && ([self.endTime isEqualToDate:other.endTime])){
            return YES;
        }
        return NO;
    }
    else if (![object isKindOfClass:self.class]) {
        return NO;
    }
    return NO;
}

-(void)parseDateToInt:(NSDate*)beginTime andWithEndTime:(NSDate*)endTime{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"Asia/Jerusalem"]];
    _month = [calendar component:NSCalendarUnitMonth fromDate:beginTime];
    _date = [calendar component:NSCalendarUnitDay fromDate:beginTime];
    _day = [calendar component:NSCalendarUnitWeekday fromDate:beginTime];
    _beginHour = [calendar component:NSCalendarUnitHour fromDate:beginTime];
    _beginMinute = [calendar component:NSCalendarUnitMinute fromDate:beginTime];
    _endHour = [calendar component:NSCalendarUnitHour fromDate:endTime];
    _endMinute = [calendar component:NSCalendarUnitMinute fromDate:endTime];
}

@end
