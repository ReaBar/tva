//  ModelParse.m
//  Tva
//
//  Created by Admin on 12/26/15.
//  Copyright © 2015 Admin. All rights reserved.
#import "ModelParse.h"
#import "Constants.h"

@implementation ModelParse
-(id)init{
    self = [super init];
    if (self) {
        [Parse setApplicationId:@"A8EaRabiTUhDiMnXJ6tC7H8QMgwViB0TEiXZtt72"
                      clientKey:@"sGwydNIeXBRBfu1QLTzskKjxmxglW0GmNP1Sx9aQ"];
    }
    return self;
}


-(BOOL)login:(NSString*)user pwd:(NSString*)pwd{
    NSError* error;
    PFUser* puser = [PFUser logInWithUsername:[user lowercaseString] password:pwd error:&error];
    if (error == nil && puser != nil) {
        return YES;
    }
    return NO;
}
-(BOOL)signup:(NSString*)user pwd:(NSString*)pwd {
    NSError* error;
    PFUser* puser = [PFUser user];
    puser.username = [user lowercaseString];
    puser.password = pwd;
    return [puser signUp:&error];
}
-(NSString*)getCurrentUser{
    PFUser* user = [PFUser currentUser];
    if (user.username != nil) {
        return [user.username lowercaseString];
    }else{
        return nil;
    }
}
-(BOOL)logout{
    [PFUser logOut];
    PFUser *currentUser = [PFUser currentUser];
    if (currentUser!=nil) {
        return NO;
    }
    else
        return YES;
}
-(void)addNewUser:(NSString*)userName withPassword:(NSString*)password{
    PFUser* obj = [PFUser user];
    obj.password = password;
    obj.username = [userName lowercaseString];
    [obj signUp];
}
-(void)addUser:(User *)userName{
    PFUser* obj = [PFUser user];
    obj.password = userName.password;
    obj.username = [userName.userName lowercaseString];
    [obj signUp];
}
-(void)deleteUser:(User*)userName{
    PFQuery* query = [PFQuery queryWithClassName:@"User"];
    [query whereKey:@"username" equalTo:userName.userName];
    NSArray* res = [query findObjects];
    if (res.count == 1) {
        PFUser* obj = [res objectAtIndex:0];
        [obj delete];
    }
}
//-----optional to delete----///
-(User*)getUser:(NSString *)userName{
    User* user = nil;
    PFQuery* query = [PFQuery queryWithClassName:@"User"];
    [query whereKey:@"username" equalTo:[userName lowercaseString]];
    NSArray* res = [query findObjects];
    if (res.count == 1) {
        PFUser* obj = [res objectAtIndex:0];
        user = [[User alloc] init:obj.username password:obj.password];
    }
    return user;
}
-(NSArray*)getUsers{
    NSMutableArray* array = [[NSMutableArray alloc] init];
    PFQuery *query = [PFQuery queryWithClassName:@"User"];
    NSArray* res = [query findObjects];
    for (PFUser* obj in res) {
        User*  user = [[User alloc] init:obj.username password:obj.password];
        [array addObject:user];
    }
    return array;
}
-(User*)getUserDetails:(NSString*)userName password:(NSString*)passWord{
    User* user = nil;
    PFQuery* query = [PFQuery queryWithClassName:@"User"];
    [query whereKey:@"username" equalTo:[userName lowercaseString]];
    [query whereKey:@"password" equalTo:passWord];
    NSArray* res = [query findObjects];
    if (res.count == 1) {
        PFUser* obj = [res objectAtIndex:0];
        user = [[User alloc] init:obj.username password:obj.password];
    }
    return user;
}

-(void)saveImage:(UIImage*)image withName:(NSString*)imageName{
    PFUser* user = [PFUser currentUser];
    NSData* imageData = UIImageJPEGRepresentation(image,0);
    PFFile* file = [PFFile fileWithName:imageName data:imageData];
    PFQuery* query = [PFQuery queryWithClassName:@"userSettings"];
    [query whereKey:@"username" equalTo:user.username];
    NSArray* res = [query findObjects];
    if (res.count == 1) {
        PFObject *fileObj = [res objectAtIndex:0];
        fileObj[@"imageName"] = imageName;
        fileObj[@"file"] = file;
        [fileObj save];
    }
    else{
        PFObject* fileObj = [PFObject objectWithClassName:@"userSettings"];
        fileObj[@"username"] = user.username;
        fileObj[@"imageName"] = imageName;
        fileObj[@"file"] = file;
        [fileObj save];
    }
}
-(UIImage*)getImage:(NSString*)imageName{
    @synchronized(self) {
        PFQuery* query = [PFQuery queryWithClassName:@"userSettings"];
        [query whereKey:@"username" equalTo:imageName];
        PFObject* obj = [query getFirstObject];
        PFFile* file = obj[@"file"];
        NSData* data = [file getData];
        UIImage* image = [UIImage imageWithData:data];
        return image;
    }
}
-(void)addCable:(NSString*)cable{
    PFQuery* query = [PFQuery queryWithClassName:@"userSettings"];
    [query whereKey:@"username" equalTo:[self getCurrentUser]];
    NSArray* res = [query findObjects];
    if (res.count == 1) {
        PFObject* obj = [res objectAtIndex:0];
        obj[@"cable"]=cable;
        [obj save];
    }
}

-(NSArray*)getPrograms{
    PFUser* user= [PFUser currentUser];
    NSMutableArray* array = [[NSMutableArray alloc] init];
    PFQuery* query = [PFQuery queryWithClassName:@"userSettings"];
    [query whereKey:@"username" equalTo:[user.username lowercaseString]];
    PFQuery* query2=[PFQuery queryWithClassName:@"Programs"];
    [query2 whereKey:@"cable" matchesKey:@"cable" inQuery:query];
    [query2 whereKey:@"cable" equalTo:@"HOT"];
    [query2 orderByAscending:@"startTime"];
    NSArray* res=[query2 findObjects];
    NSArray *descriptor = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"begin" ascending:YES]];
    NSArray *sortedArray = [res sortedArrayUsingDescriptors:descriptor];
    for ( PFObject* obj in sortedArray) {
        Program* pr = [[Program alloc] initWithName:obj[@"name"] withObjectId:obj.objectId cable:obj[@"cable"] channel:obj[@"channel"] genre:obj[@"genre"] beginTime:obj[@"startTime"] endTime:obj[@"endTime"]];
        [array addObject:pr];
    }
    return array;
}
-(NSArray*)getProgramsWithNSDateFormat{
    PFUser* user = [PFUser currentUser];
    NSMutableArray* array = [[NSMutableArray alloc]init];
    PFQuery* query = [PFQuery queryWithClassName:@"userSettings"];
    [query whereKey:@"username" equalTo:[user.username lowercaseString]];
    PFQuery* query2=[PFQuery queryWithClassName:@"Programs"];
    [query2 whereKey:@"cable" matchesKey:@"cable" inQuery:query];
    [query2 whereKey:@"cable" equalTo:@"HOT"];
    NSArray* res=[query2 findObjects];
    NSArray *descriptor = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"startTime" ascending:YES]];
    NSArray *sortedArray = [res sortedArrayUsingDescriptors:descriptor];
    for ( PFObject* obj in sortedArray) {
        //        NSString* programGenre = obj[@"genre"];
        //        NSString* tempGenre = [ModelParse programGenre:programGenre];
        Program* pr = [[Program alloc] initWithName:obj[@"name"] withObjectId:obj.objectId cable:obj[@"cable"] channel:obj[@"channel"] genre:obj[@"genre"] beginTime:obj[@"startTime"] endTime:obj[@"endTime"]];
        [array addObject:pr];
    }
    return array;
}
-(NSArray*)getUserProgramsByHour:(NSString*)begin end:(NSString*)end{
    PFUser* user=[PFUser currentUser];
    NSMutableArray* array = [[NSMutableArray alloc] init];
    PFQuery* query = [PFQuery queryWithClassName:@"userSettings"];
    [query whereKey:@"username" equalTo:[user.username lowercaseString]];
    PFQuery* query2=[PFQuery queryWithClassName:@"Programs"];
    [query2 whereKey:@"cable" matchesKey:@"cable" inQuery:query];
    [query2 whereKey:@"begin" equalTo:begin];
    [query2  whereKey:@"end" equalTo:end];
    NSArray* res=[query2 findObjects];
    for ( PFObject* obj in res) {
        Program* pr = [[Program alloc] initWithName:obj[@"name"] withObjectId:obj.objectId cable:obj[@"cable"] channel:obj[@"channel"] genre:obj[@"genre"] beginTime:obj[@"startTime"] endTime:obj[@"endTime"]];
        [array addObject:pr];
    }
    PFQuery* query3= [PFQuery queryWithClassName:@"Programs"];
    [query3 whereKey:@"cable" equalTo:@"national"];
    [query3 whereKey:@"begin" equalTo:begin];
    [query3  whereKey:@"end" equalTo:end];
    NSArray* res3=[query3 findObjects];
    for ( PFObject* obj in res3) {
        Program* pr = [[Program alloc] initWithName:obj[@"name"] withObjectId:obj.objectId cable:obj[@"cable"] channel:obj[@"channel"] genre:obj[@"genre"] beginTime:obj[@"startTime"] endTime:obj[@"endTime"]];
        [array addObject:pr];
    }
    return array;
}

-(NSArray*)getUserProgramsByHour:(NSDate*)begin{
    PFUser* user=[PFUser currentUser];
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"Asia/Jerusalem"];
    NSString* beginHour = [formatter stringFromDate:begin];
    NSDate* searchTime = [formatter dateFromString:beginHour];
    NSLog(@"Begin Hour: %@",beginHour);
    NSMutableArray* array = [[NSMutableArray alloc] init];
    PFQuery* query = [PFQuery queryWithClassName:@"userSettings"];
    [query whereKey:@"username" equalTo:[user.username lowercaseString]];
    PFQuery* query2=[PFQuery queryWithClassName:@"Programs"];
    [query2 whereKey:@"cable" matchesKey:@"cable" inQuery:query];
    [query2 whereKey:@"endTime" greaterThan:searchTime];
    [query2 orderByAscending:@"startTime"];
    NSArray* res=[query2 findObjects];
    for ( PFObject* obj in res) {
        Program* pr = [[Program alloc] initWithName:obj[@"name"] withObjectId:obj.objectId cable:obj[@"cable"] channel:obj[@"channel"] genre:obj[@"genre"] beginTime:obj[@"startTime"] endTime:obj[@"endTime"]];
        [array addObject:pr];
    }
    PFQuery* query3= [PFQuery queryWithClassName:@"Programs"];
    [query3 whereKey:@"cable" equalTo:@"national"];
    [query3 whereKey:@"startTime" greaterThan:searchTime];
    NSArray* res3=[query3 findObjects];
    for ( PFObject* obj in res3) {
        Program* pr = [[Program alloc] initWithName:obj[@"name"] withObjectId:obj.objectId cable:obj[@"cable"] channel:obj[@"channel"] genre:obj[@"genre"] beginTime:obj[@"startTime"] endTime:obj[@"endTime"]];
        [array addObject:pr];
    }
    return array;
}

-(void)addProgramToUser:(Program*)program{
    _checkValidUserProgram=NO;
    PFUser* user=[PFUser currentUser];
    PFObject* obj = [PFObject objectWithClassName:@"userPrograms"];
    PFQuery* query=[PFQuery queryWithClassName:@"userPrograms"];
    [query whereKey:@"username" equalTo:user.username];
    [query whereKey:@"program" equalTo:program.name];
    [query whereKey:@"startTime" equalTo:program.startTime];
    [query whereKey:@"endTime" equalTo:program.endTime];
    [query whereKey:@"channel" equalTo:program.channel];
    //    [query whereKey:@"begin" equalTo:program.begin];
    //    [query whereKey:@"end" equalTo:program.end];
    NSArray *res = [query findObjects];
    if(res.count == 0){
        obj[@"username"]=user.username;
        obj[@"program"]=program.name;
        obj[@"startTime"] = program.startTime;
        obj[@"endTime"] = program.endTime;
        obj[@"channel"]=program.channel;
        //        obj[@"begin"]=program.begin;
        //        obj[@"end"]=program.end;
        _checkValidUserProgram=YES;
        [obj save];
    }
}

-(void)deleteProgramFromUser:(Program*)program{
    PFUser* user = [PFUser currentUser];
    //PFObject* obj = [PFObject objectWithClassName:@"userPrograms"];
    PFQuery* query = [PFQuery queryWithClassName:@"userPrograms"];
    [query whereKey:@"username" equalTo:user.username];
    [query whereKey:@"program" equalTo:program.name];
    [query whereKey:@"startTime" equalTo:program.startTime];
    [query whereKey:@"endTime" equalTo:program.endTime];
    NSArray* res = [query findObjects];
    if(res.count != 0){
        PFObject* deleteObj = res[0];
        [deleteObj delete];
    }
}

-(NSArray*)getUserPrograms{
    PFUser* user=[PFUser currentUser];
    NSMutableArray* array = [[NSMutableArray alloc] init];
    PFQuery* query = [PFQuery queryWithClassName:@"userPrograms"];
    [query whereKey:@"username" equalTo:[user.username lowercaseString]];
    
    NSArray* res=[query findObjects];
    for (PFObject* obj in res) {
        Program* pr= [[Program alloc] initWithUserName:user.username withProgramName:obj[@"program"] withObjectId:obj.objectId cable:obj[@"cable"] channel:obj[@"channel"] genre:obj[@"genre"] beginTime:obj[@"startTime"] endTime:obj[@"endTime"]];
        [array addObject:pr];
    }
    //    PFQuery* query2 = [PFQuery queryWithClassName:@"userFriends"];
    //    [query2 whereKey:@"username" matchesKey:@"username" inQuery:query];
    //    PFQuery* query3 = [PFQuery queryWithClassName:@"userPrograms"];
    //    [query3 whereKey:@"username" matchesKey:@"friend" inQuery:query2];
    //    NSArray* res2=[query3 findObjects];
    //    for ( PFObject* obj2 in res2) {
    //        Program* pr2= [[Program alloc] initWithUserName:user.username withProgramName:obj2[@"program"] withObjectId:obj2.objectId cable:obj2[@"cable"] channel:obj2[@"channel"] genre:obj2[@"genre"] beginTime:obj2[@"startTime"] endTime:obj2[@"endTime"]];
    //        [array addObject:pr2];
    //    }
    return array;
}

-(User*)parseUser{
    PFUser* parseUser = [PFUser currentUser];
    PFQuery* query = [PFQuery queryWithClassName:@"userSettings"];
    [query whereKey:@"username" equalTo:parseUser.username];
    User *user = [[User alloc]init];
    user.userName = parseUser.username;
    NSArray *res = [query findObjects];
    PFObject *pfObj = res[0];
    if (pfObj[@"cable"]!=nil) {
        user.cable = pfObj[@"cable"];
    }
    if(pfObj[@"imageName"]!=nil){
        user.imageName = pfObj[@"imageName"];
    }
    if(pfObj[@"pic"]!=nil){
        
    }
    if(pfObj[@"programs"]!=nil){
        
    }
    return user;
}
-(NSArray*)getProgramsFromDate:(NSString*)date{
    NSMutableArray* array = [[NSMutableArray alloc] init];
    PFQuery* query = [PFQuery queryWithClassName:@"Programs"];
    NSDate* dated = [NSDate dateWithTimeIntervalSince1970:[date doubleValue]];
    [query whereKey:@"updatedAt" greaterThanOrEqualTo:dated];
    NSArray* res = [query findObjects];
    NSArray *descriptor = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"begin" ascending:YES]];
    NSArray *sortedArray = [res sortedArrayUsingDescriptors:descriptor];
    for (PFObject* obj in sortedArray) {
        Program* pr = [[Program alloc] initWithName:obj[@"name"] withObjectId:obj.objectId cable:obj[@"cable"] channel:obj[@"channel"] genre:obj[@"genre"] beginTime:obj[@"startTime"] endTime:obj[@"endTime"]];
        [array addObject:pr];
    }
    return array;
}


-(NSArray*)getUserProgramsFromDate:(NSString*)date{
    PFUser* user=[PFUser currentUser];
    NSMutableArray* array = [[NSMutableArray alloc] init];
    PFQuery* query = [PFQuery queryWithClassName:@"userPrograms"];
    [query whereKey:@"username" equalTo:[user.username lowercaseString]];
    NSDate* dated = [NSDate dateWithTimeIntervalSince1970:[date doubleValue]];
    [query whereKey:@"updatedAt" greaterThanOrEqualTo:dated];
    NSArray* res=[query findObjects];
    for ( PFObject* obj in res) {
        Program* pr = [[Program alloc] initWithName:obj[@"name"] withObjectId:obj.objectId cable:obj[@"cable"] channel:obj[@"channel"] genre:obj[@"genre"] beginTime:obj[@"startTime"] endTime:obj[@"endTime"]];
        [array addObject:pr];
    }
    return array;
}

-(void)addUserToDB:(NSString*)city age:(NSString*)age gender:(NSString*)gender drama:(BOOL)drama comedy:(BOOL)comedy action:(BOOL)action doco:(BOOL)doco sport:(BOOL)sport horror:(BOOL)horror{
    PFUser* user = [PFUser currentUser];
    PFQuery* query = [PFQuery queryWithClassName:@"userSettings"];
    [query whereKey:@"username" equalTo:user.username];
    NSArray *res = [query findObjects];
    if(res.count == 0){
        PFObject* fileObj = [PFObject objectWithClassName:@"userSettings"];
        fileObj[@"username"] = user.username;
        fileObj[@"city"]=city;
        fileObj[@"age"]=age;
        fileObj[@"gender"]=gender;
        fileObj[@"drama"]=@(drama);
        fileObj[@"comedy"]=@(comedy);
        fileObj[@"horror"]=@(horror);
        fileObj[@"action"]=@(action);
        fileObj[@"sport"]=@(sport);
        fileObj[@"doco"]=@(doco);
        [fileObj save];
    }
}

-(NSDate*)getTableLastUpdateTime:(NSString*)tableName{
    PFQuery *query = [PFQuery queryWithClassName:[NSString stringWithFormat:@"%@",tableName]];
    [query orderByDescending:@"updatedAt"];
    PFObject* obj = [query getFirstObject];
    return obj.updatedAt;
}

-(NSDate*)getUserLastUpdate:(NSString*)username{
    PFQuery *query = [PFQuery queryWithClassName:[NSString stringWithFormat:@"userSettings"]];
    [query whereKey:@"username" equalTo:username];
    PFObject* obj = [query getFirstObject];
    return obj.updatedAt;
}
-(BOOL)checkUserProgram{
    return (_checkValidUserProgram);
}
-(void)addAutomaticSystem: (BOOL)autoButton{
    PFQuery* query = [PFQuery queryWithClassName:@"userSettings"];
    [query whereKey:@"username" equalTo:[self getCurrentUser]];
    NSArray* res = [query findObjects];
    if (res.count == 1) {
        PFObject* obj = [res objectAtIndex:0];
        obj[@"automaticButton"]=@(autoButton);
        [obj save];
    }
    
}

+(NSString*)programGenre:(NSString*)genre{
    if([genre isEqualToString:SPORT])
        return @"sport";
    else if ([genre isEqualToString:CURRENT_AFFAIRS_AND_NEWS])
        return @"current affairs and news";
    else if([genre isEqualToString:CHILDREN])
        return @"children";
    else if([genre isEqualToString:CULTURE_ENTERTAINMENT_AND_MUSIC])
        return @"culture entertainment and music";
    else if([genre isEqualToString:DOCUMENTARY_NATURE_AND_LEISURE])
        return @"documentary nature and leisure";
    else if([genre isEqualToString:SERIES])
        return @"series";
    else if([genre isEqualToString:MOVIE])
        return @"movie";
    return NULL;
}

-(void)addAutomaticPrograms{
    NSString* userProfile=nil;
    PFUser* user=[PFUser currentUser];
    //  NSMutableArray* array = [[NSMutableArray alloc] init];
    PFQuery* query = [PFQuery queryWithClassName:@"userSettings"];
    [query whereKey:@"username" equalTo:[user.username lowercaseString]];
    NSArray* res = [query findObjects];
    if (res.count == 1) {
        PFObject* obj = [res objectAtIndex:0];
        userProfile=obj[@"profile"];
        PFQuery* query2 = [PFQuery queryWithClassName:@"userSettings"];
        [query2 whereKey:@"profile" equalTo:userProfile];
        NSArray* res2 = [query2 findObjects];
        if(res2.count!=0){
            PFQuery* query3 = [PFQuery queryWithClassName:@"userPrograms"];
            //[query3 whereKey:@"profile" matchesKey:@"profile" inQuery:query2];
            [query3 whereKey:@"username" notEqualTo:user.username];
            NSArray* res3 = [query3 findObjects];
            if(res3.count!=0 && res.count>=3){
                for(int i=0;i<3;i++){
                    PFObject* obj2 = [res3 objectAtIndex:i];
                     PFObject* fileObj = [PFObject objectWithClassName:@"userPrograms"];
                    fileObj[@"username"]=user.username;
                    fileObj[@"program"]=obj2[@"program"];
                    fileObj[@"startTime"] = obj2[@"startTime"];
                    fileObj[@"endTime"] = obj2[@"endTime"];
                    fileObj[@"channel"]=obj2[@"channel"];
                    NSString* temp=obj[@"program"];
                    PFQuery* query4 = [PFQuery queryWithClassName:@"userPrograms"];
                    [query4 whereKey:@"username" equalTo:user.username];
                    [query4 whereKey:@"program" containsString:temp];
                    NSArray* res4 = [query4 findObjects];
                    if(res4.count==0){
                        [fileObj save];
                    }
                    else{
                        continue;
                    }

                }
                
            }
            else if(res3.count!=0 && res.count<3){
                for(PFObject* obj3 in res3){
                    PFObject* fileObj = [PFObject objectWithClassName:@"userPrograms"];
                   fileObj[@"username"]= user.username;
                   fileObj[@"program"]=obj3[@"program"];
                   fileObj[@"startTime"]=obj3[@"startTime"];
                    fileObj[@"endTime"]=obj3[@"endTime"];
                    fileObj[@"channel"]=obj3[@"channel"];
                    NSString* temp=obj3[@"program"];
                    PFQuery* query4 = [PFQuery queryWithClassName:@"userPrograms"];
                    [query4 whereKey:@"username" equalTo:user.username];
                    [query4 whereKey:@"program" containsString:temp];
                     NSArray* res4 = [query4 findObjects];
                    if(res4.count==0){
                    [fileObj save];
                    }
                    else{
                        continue;
                    }
                }
                
            }
        }
        
    }
}

@end
