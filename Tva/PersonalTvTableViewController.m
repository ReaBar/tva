//
//  PersonalTvTableViewController.m
//  Tva
//
//  Created by Admin on 12/24/15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import "PersonalTvTableViewController.h"

@interface PersonalTvTableViewController ()

@end

@implementation PersonalTvTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    self.tableView.backgroundColor = [UIColor darkGrayColor];
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewDidAppear:(BOOL)animated{
    if(animated==YES){
        [[Model instance] getUserProgramsAsynch:^(NSMutableArray * array) {
            self.programsData=array;
            [self.tableView reloadData];
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return _programsData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Program* pr= [_programsData objectAtIndex:indexPath.row];
    NSDate* sourceDate = [NSDate date];
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"Asia/Jerusalem"];
    NSString* startTime = [formatter stringFromDate:sourceDate];
    NSDate* currentDate = [formatter dateFromString:startTime];
    PersonalTvTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PersonalTvTableViewCell" forIndexPath:indexPath];
    if([pr.endTime isEqualToDate:[pr.endTime laterDate:currentDate]]){
        cell.name.text=pr.name;
        NSInteger b = [pr.channel integerValue];
        if (b<58 || b==60 || b==69 || b==77|| b==84) {
            NSString* imageProgram= [NSString stringWithFormat:@"%@.png",pr.channel];
            cell.imageView.image = [UIImage imageNamed:imageProgram];
        }
        else{
            NSString* imageProgram= [NSString stringWithFormat:@"hot.png"];
            cell.imageView.image = [UIImage imageNamed:imageProgram];
        }

    }
    
    // Configure the cell...
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        /// Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view///
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove the row from data model
  //  PersonalTvTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PersonalTvTableViewCell" forIndexPath:indexPath];
    Program* deleteFavorite= [_programsData objectAtIndex:indexPath.row]; ;
    [[Model instance] deleteProgramFromUser:deleteFavorite block:^(NSError* er){
    }];
    [_programsData removeObject:deleteFavorite];
    [self.tableView reloadData];
}

- (IBAction)backButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
