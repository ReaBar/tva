//
//  TvTableViewController.m
//  Tva
//
//  Created by Admin on 12/10/15.
//  Copyright © 2015 Admin. All rights reserved.
//

#import "TvTableViewController.h"
#import "TvTableViewCell.h"
#import "Program.h"
#import "Model.h"

@interface TvTableViewController ()
@end

@implementation TvTableViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    [[Model instance] getUserProgramsAsynch:^(NSMutableArray * array) {
        self.userPrograms=array;
    }];
    
    [[Model instance] getUserProgramsByHourAsynch:[self getCurrentDate] block:^(NSArray* array){
        self.programsData = array;
    }];
    
//    [[Model instance] getProgramsAsynch:^(NSArray * array) {
//        self.programsData=array;
//        [self.tableView reloadData];
//    }];
   }

-(void)viewDidAppear:(BOOL)animated{
//    [[Model instance] getProgramsAsynch:^(NSArray* array){
//        if(self.programsData.count != array.count){
//            self.programsData = array;
//        };
//    }];
    
    [[Model instance] getUserProgramsByHourAsynch:[self getCurrentDate] block:^(NSArray* array){
        self.programsData = array;
    }];
    
    [[Model instance] getUserProgramsAsynch:^(NSMutableArray * array) {
        self.userPrograms = array;
        [self.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _programsData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TvTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TvTableViewCell" forIndexPath:indexPath];
    Program* pr= [_programsData objectAtIndex:indexPath.row];
    NSDate* sourceDate = [NSDate date];
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"Asia/Jerusalem"];
    NSString* startTime = [formatter stringFromDate:sourceDate];
    NSDate* currentDate = [formatter dateFromString:startTime];
    if([pr.endTime isEqualToDate:[pr.endTime laterDate:currentDate]]){
        if(_userPrograms.count != 0){
            for (int i=0; i<_userPrograms.count; i++) {
                Program* userPr=[_userPrograms objectAtIndex:i];
                if([pr.name isEqualToString:userPr.name]){
                    [cell setBackgroundColor:[UIColor orangeColor]];
                    cell.contentView.backgroundColor = [UIColor orangeColor];
                    NSInteger b = [pr.channel integerValue];
                    if (b<58 || b==60 || b==69 || b==77|| b==84) {
                        NSString* imageProgram= [NSString stringWithFormat:@"%@.png",pr.channel];
                        cell.imageView.image = [UIImage imageNamed:imageProgram];
                    }
                    else{
                        NSString* imageProgram= [NSString stringWithFormat:@"hot.png"];
                        cell.imageView.image = [UIImage imageNamed:imageProgram];
                    }

                    cell.name.text=pr.name;
                    cell.hour.text=[NSString stringWithFormat:@" %02ld:%02ld-%02ld:%02ld",
                                    pr.beginHour,pr.beginMinute,pr.endHour,pr.endMinute];
                    
                    cell.accessoryView = [[ UIImageView alloc ]
                                          initWithImage:[UIImage imageNamed:@"detailDisclosure"]];
                    return cell;
                }
            }
        }
        [cell setBackgroundColor:[UIColor grayColor]];
        cell.contentView.backgroundColor = [UIColor grayColor];
        cell.name.text=pr.name;
        cell.hour.text=[NSString stringWithFormat:@" %02ld:%02ld-%02ld:%02ld",
                        pr.beginHour,pr.beginMinute,pr.endHour,pr.endMinute];
        NSInteger b = [pr.channel integerValue];
        if (b<58 || b==60 || b==69 || b==77|| b==84) {
            NSString* imageProgram= [NSString stringWithFormat:@"%@.png",pr.channel];
            cell.imageView.image = [UIImage imageNamed:imageProgram];
        }
        else{
            NSString* imageProgram= [NSString stringWithFormat:@"hot.png"];
            cell.imageView.image = [UIImage imageNamed:imageProgram];
        }

        cell.accessoryView = [[ UIImageView alloc ]
                              initWithImage:[UIImage imageNamed:@"detailDisclosure"]];
    }
    else{
        
    }
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
/*- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 
 
 
 }*/


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _favoriteProgram = [_programsData objectAtIndex:indexPath.row];
    if(![_userPrograms containsObject:_favoriteProgram]){
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@""
                                      message:@"Do you want to set this program as a favorite?"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [[Model instance] addProgramToUserAsynch:_favoriteProgram block:^(NSError* er){
                                     UITableViewCell *selectedCell=[tableView cellForRowAtIndexPath:indexPath];
                                     [selectedCell setSelectionStyle:UITableViewCellSelectionStyleBlue];
                                 }];

                                 [self.userPrograms addObject:_favoriteProgram];
                                 [self.tableView reloadData];
                                 UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                                 localNotification.fireDate = _favoriteProgram.startTime;
                                 localNotification.alertBody = [NSString stringWithFormat:@"%@ משודר כעת בערוץ %@",_favoriteProgram.name, _favoriteProgram.channel];
                                 localNotification.alertAction = @"Show me the program";
                                 localNotification.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
                                 localNotification.soundName = UILocalNotificationDefaultSoundName;
                                 localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
                                 [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [tableView deselectRowAtIndexPath:indexPath animated:YES];
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    
    else{
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:@""
                                      message:@"Do you want to remove this program from favorites?"
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"YES"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [[Model instance] deleteProgramFromUser:_favoriteProgram block:^(NSError* er){
                                     UITableViewCell *selectedCell=[tableView cellForRowAtIndexPath:indexPath];
                                     [selectedCell setSelectionStyle:UITableViewCellSelectionStyleBlue];
                                 }];
                                 [self.userPrograms removeObject:_favoriteProgram];
                                 [self.tableView reloadData];
                                 NSArray* arrayOfNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
                                 for(UILocalNotification *localNotification in arrayOfNotifications){
                                     if([localNotification.fireDate isEqualToDate:_favoriteProgram.startTime] && [localNotification.alertBody isEqualToString:[NSString stringWithFormat:@"%@ משודר כעת בערוץ %@",_favoriteProgram.name, _favoriteProgram.channel]]){
                                         [[UIApplication sharedApplication] cancelLocalNotification:localNotification];
                                     }
                                 }
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        UIAlertAction* cancel = [UIAlertAction
                                 actionWithTitle:@"NO"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                     [tableView deselectRowAtIndexPath:indexPath animated:YES];
                                     [alert dismissViewControllerAnimated:YES completion:nil];
                                 }];
        
        [alert addAction:ok];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
    }
}



- (void)changeDate:(UIDatePicker *)sender {
    _datePicked = sender.date;
}

- (void)removeViews:(id)object {
    [[self.view viewWithTag:9] removeFromSuperview];
    [[self.view viewWithTag:10] removeFromSuperview];
    [[self.view viewWithTag:11] removeFromSuperview];
}

- (void)dismissDatePicker:(id)sender {
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height+44, 320, 216);
    [UIView beginAnimations:@"MoveOut" context:nil];
    [self.view viewWithTag:9].alpha = 0;
    [self.view viewWithTag:10].frame = datePickerTargetFrame;
    [self.view viewWithTag:11].frame = toolbarTargetFrame;
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(removeViews:)];
    [UIView commitAnimations];
}

- (IBAction)editTime:(id)sender {
    UIView *viewDatePicker = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 200)];
    [viewDatePicker setBackgroundColor:[UIColor clearColor]];
    
    //Make a frame for the picker & then create the picker
    CGRect pickerFrame = CGRectMake(0, 0, self.view.frame.size.width, 200);
    _datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
    
    _datePicker.datePickerMode= UIDatePickerModeDateAndTime;;
    _datePicker.hidden = NO;
    _datePicker.minimumDate=  [self getCurrentDate];
    [viewDatePicker addSubview:_datePicker];
    
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil
                                                                                 message:@"\n\n\n\n\n\n\n\n\n\n"
                                                                          preferredStyle:UIAlertControllerStyleActionSheet];
        
        [alertController.view addSubview:viewDatePicker];
        
        UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                     {
                                         _datePicked = _datePicker.date;
                                        [[Model instance] getUserProgramsByHourAsynch:_datePicked block:^(NSArray * array) {
                                        self.programsData=array;
                                        [self.tableView reloadData];
                                         }];

                                        [self dismissDatePicker:sender];
                                         NSLog(@"OK action");
                                         
                                     }];
        [alertController addAction:doneAction];
        
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action)
                                       {
                                           NSLog(@"Cancel action");
                                       }];
        
        
        
        [alertController addAction:cancelAction];
        
        /*
         UIAlertAction *resetAction = [UIAlertAction
         actionWithTitle:NSLocalizedString(@"Reset", @"Reset action")
         style:UIAlertActionStyleDestructive
         handler:^(UIAlertAction *action)
         {
         NSLog(@"Reset action");
         }];
         
         [alertController addAction:resetAction];
         */
        
        
        [self presentViewController:alertController animated:YES completion:nil];
        
        
        
    }
    else
    {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Done" otherButtonTitles:nil, nil];
        [actionSheet addSubview:viewDatePicker];
        [actionSheet showInView:self.view];
        
    }
    



        
    
    
/*      if ([self.view viewWithTag:9]) {
        return;
    }
    CGRect toolbarTargetFrame = CGRectMake(0, self.view.bounds.size.height-216-44, 320, 44);
    CGRect datePickerTargetFrame = CGRectMake(0, self.view.bounds.size.height-216, 320, 216);
    
    UIView *darkView = [[UIView alloc] initWithFrame:self.view.bounds];
    darkView.alpha = 0;
    darkView.backgroundColor = [UIColor lightGrayColor];
    darkView.tag = 9;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissDatePicker:)];
    [darkView addGestureRecognizer:tapGesture];
    [self.view addSubview:darkView];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height+44, 320, 216)];
    [datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
    datePicker.tag = 10;
    datePicker.backgroundColor = [UIColor lightGrayColor];
    
    [datePicker addTarget:self action:@selector(changeDate:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:datePicker];
    
    _datePicked = datePicker.date;
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height, 320, 44)];
    toolBar.tag = 11;
    toolBar.barStyle = UIBarStyleBlackTranslucent;
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonClicked:)];
    [toolBar setItems:[NSArray arrayWithObjects:spacer, doneButton, nil]];
    [self.view addSubview:toolBar];
    
    [UIView beginAnimations:@"MoveIn" context:nil];
    toolBar.frame = toolbarTargetFrame;
    datePicker.frame = datePickerTargetFrame;
    darkView.alpha = 0.5;
    [UIView commitAnimations];
*/

    
}

-(void)doneButtonClicked:(id)sender{
    [self dismissDatePicker:sender];
    [[Model instance] getUserProgramsByHourAsynch:_datePicked block:^(NSArray * array) {
        self.programsData=array;
        [self.tableView reloadData];
    }];
}

/*-(void)viewDidUnload{
    self.datePickerDemo=nil;
    [super viewDidUnload];
}*/

-(NSDate*)getCurrentDate{
    NSDate* sourceDate = [NSDate date];
    NSDateFormatter* formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    formatter.timeZone = [NSTimeZone timeZoneWithName:@"Asia/Jerusalem"];
    NSString* startTime = [formatter stringFromDate:sourceDate];
    NSDate* currentDate = [formatter dateFromString:startTime];
    
    return currentDate;
}

@end
