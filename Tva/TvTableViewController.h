//
//  TvTableViewController.h
//  Tva
//
//  Created by Admin on 12/10/15.
//  Copyright © 2015 Admin. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "User.h"
#import "Program.h"
#import "User.h"

@interface TvTableViewController : UITableViewController <UIActionSheetDelegate,UIAlertViewDelegate>
- (IBAction)editTime:(id)sender;
- (void)dismissDatePicker:(id)sender;
- (void)removeViews:(id)object;
- (void)changeDate:(UIDatePicker *)sender;
- (void)doneButtonClicked:(id)sender;
-(NSDate*)getCurrentDate;

@property(nonatomic) NSArray *timeData;
@property NSArray* programsData;
@property NSMutableArray* userPrograms;
@property NSString* editTime;
@property Program* favoriteProgram;
@property (nonatomic) NSString* timeSelected;
@property User* user;
@property NSDate *datePicked;
@property UILabel *label;
@property UIDatePicker *datePicker;

@property BOOL showPickedFinal;
@end
