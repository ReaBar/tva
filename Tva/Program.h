//
//  Program.h
//  Tva
//
//  Created by Admin on 12/10/15.
//  Copyright © 2015 Admin. All rights reserved.
///

#import <Foundation/Foundation.h>

@interface Program : NSObject
@property NSString* objectId;
@property NSString* name;
@property NSString* channel;
@property NSString* cable;
@property NSString* userName;
@property NSString* genre;
@property NSDate* startTime;
@property NSDate* endTime;
@property NSInteger beginHour;
@property NSInteger endHour;
@property NSInteger beginMinute;
@property NSInteger endMinute;
@property NSInteger day;
@property NSInteger date;
@property NSInteger month;

-(id)initWithName:(NSString*)name withObjectId:(NSString*)objectId cable:(NSString*)cable channel:(NSString*)channel genre:(NSString*)genre beginTime:(NSDate*)begin endTime:(NSDate*)end;
-(id)initWithUserName:(NSString*)username withProgramName:(NSString*)programName withObjectId:(NSString*)objectId cable:(NSString*)cable channel:(NSString*)channel genre:(NSString*)genre beginTime:(NSDate*)begin endTime:(NSDate*)end;
//-(id)init:(NSString*)username withObjectId:(NSString*)objectId programeName:(NSString*)name beginTime:(NSString*)begin endTime:(NSString*)end;
- (BOOL)isEqual:(id)object;
-(void)parseDateToInt:(NSDate*)beginTime andWithEndTime:(NSDate*)endTime;
@end
